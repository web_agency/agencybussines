<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>About - Business Casual - Start Bootstrap Theme</title>

    <%@ include file="fragment/header.jspf" %>
</head>

<body>
<!-- Navigation -->
<%@include file="fragment/header-navigator.jspf" %>

<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <hr>
            <h2 class="intro-text text-center">
                <strong>Продажа квартир</strong>
            </h2>
            <hr>
        </div>
<c:forEach var="flat" items="${flats}">
        <div class="box">

            <div class="col-md-6">
                <img class="img-responsive img-border-left" src="img/1.jpg" alt="">
            </div>
            <div class="col-md-6">
                <h2 class="intro-text text-center">
                    <strong>Квартира №1</strong>
                </h2>
                <p>Планировка</p>
                <p>Кол-во кв метров</p>
                <p>Район</p>
                <a href="sale_kvart.jsp" class="btn btn-primary btn-lg" type="button">Подробней</a>
            </div>
            <div class="clearfix"></div>
        </div>
</c:forEach>

    </div>
    <div class="col-lg-12 text-center">
        <ul class="pager">
            <li class="previous"><a href="#">&larr; Предыдущая</a>
            </li>
            <li class="next"><a href="#">Следующая &rarr;</a>
            </li>
        </ul>
    </div>

</div>
<%@ include file="fragment/footer.jspf"%>
</body>

</html>
