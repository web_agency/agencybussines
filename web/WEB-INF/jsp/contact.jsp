<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Contact - Business Casual - Start Bootstrap Theme</title>

    <%@ include file="fragment/header.jspf" %>
</head>

<body>

<!-- Navigation -->
<%@include file="fragment/header-navigator.jspf"%>

<div class="container">

    <div class="row">
        <div class="box">
            <div class="col-lg-12">
                <hr>
                <h2 class="intro-text text-center">Наша команда
                </h2>
                <hr>
            </div>
            <div class="col-sm-4 text-center">
                <img class="img-responsive" src="http://placehold.it/750x450" alt="">
                <h3>
                    <small>Достижения, сертификаты</small>
                </h3>
            </div>
            <div class="col-sm-4 text-center">
                <img class="img-responsive" src="http://placehold.it/750x450" alt="">
                <h3>
                    <small>Достижения, сертификаты</small>
                </h3>
            </div>
            <div class="col-sm-4 text-center">
                <img class="img-responsive" src="http://placehold.it/750x450" alt="">
                <h3>
                    <small>Достижения, сертификаты</small>
                </h3>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="row">
        <div class="box">
            <div class="col-lg-12">
                <hr>
                <h2 class="intro-text text-center">Как нас найти
                </h2>
                <hr>
            </div>
            <div class="col-md-8">

                <!-- Embedded Google Map using an iframe - to select your location find it on Google maps and paste the link as the iframe src. If you want to use the Google Maps API instead then have at it! -->
                <iframe width="100%" height="400px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1047.3202979295847!2d36.22893467920949!3d50.01225164197386!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4127a121371f2c3b%3A0xa0ab20bb457bb752!2z0L_RgNC-0YHQvy4g0JvQtdC90ZbQvdCwLCAxMiwg0KXQsNGA0LrRltCyLCDQpdCw0YDQutGW0LLRgdGM0LrQsCDQvtCx0LvQsNGB0YLRjA!5e0!3m2!1sru!2sua!4v1455196765247"></iframe>
            </div>
            <div class="col-md-4">
                <p>Телефон
                    <strong>+38(057)000 00 00</strong>
                </p>
                <p>Email:
                    <strong><a href="mailto:name@example.com">email@gmail.com</a></strong>
                </p>
                <p>Адрес:
                    <strong>проспект Науки, 12</strong>
                </p>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="row">
        <div class="box">
            <div class="col-lg-12">
                <hr>
                <h2 class="intro-text text-center">Обратная
                    <strong>связь </strong>
                </h2>
                <hr>
                <p>Здесь Вы можете оставить нам свое сообщение и мы Вам ответим в ближайшее время:</p>
                <form:form method="post" action="${pageContext.request.contextPath}/mail/feedback" commandName="message">
                    <div class="row">
                        <div class="form-group col-lg-4">
                            <label>Имя:</label>
                            <form:input path="name" cssClass="form-control"/>
                        </div>
                        <div class="form-group col-lg-4">
                            <label>Фамилия:</label>
                            <form:input path="surname" cssClass="form-control"/>
                        </div>
                        <div class="form-group col-lg-4">
                            <label>Номер телефона:</label>
                            <form:input path="phone" cssClass="form-control"/>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group col-lg-12">
                            <label>Сообщение:</label>
                            <form:textarea path="text" cssClass="form-control" rows="6"/>
                        </div>
                        <div class="form-group col-lg-12">
                            <input type="hidden" name="save" value="contact">
                            <button type="submit" class="btn btn-primary btn-lg">Отправить</button>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>

</div>
<%@ include file="fragment/footer.jspf"%>
</body>

</html>
