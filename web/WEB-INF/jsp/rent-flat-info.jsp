<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>About - Business Casual - Start Bootstrap Theme</title>

    <%@ include file="fragment/header.jspf"%>

</head>

<body>
    <%@include file="fragment/header-navigator.jspf"%>

    <div class="container">

        <div class="row">
            <div class="col-lg-12">
                    <hr>
                    <h2 class="intro-text text-center">
                        <strong>Квартира</strong>
                    </h2>
                    <hr>
                </div>
            <div class="box">
                <div class="col-md-6">
                    <div id="carousel-example-generic" class="carousel slide">
                        <!-- Indicators -->
                        <ol class="carousel-indicators hidden-xs">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <img class="img-responsive img-full" src="img/1.jpg" alt="">
                            </div>
                            <div class="item">
                                <img class="img-responsive img-full" src="img/1.jpg" alt="">
                            </div>
                            <div class="item">
                                <img class="img-responsive img-full" src="img/1.jpg" alt="">
                            </div>
                            
                        </div>
                  
                        <!-- Controls -->
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="icon-prev"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="icon-next"></span>
                        </a>
                    </div>
                </div>
                 <div class="col-md-4">
                              <p>Планировка: </p>
                              <p>Кол-во кв метров: ${flat.size}</p>
                              <p>Район: ${flat.street}</p>
                              <p>Цена в месяц: ${flat.cost}</p>
                              <%--<p>Аренда</p><br/>--%>
                     <p>Описание:</p><br/>
                     ${flat.description}
                              <a href="#" class="btn btn-primary btn-lg" type="button">Оставить заявку</a>
                            </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <%@ include file="fragment/footer.jspf"%>
</body>

</html>
