<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Creative - Bootstrap Admin Template</title>

    <!-- Bootstrap CSS -->
    <link href="${pageContext.request.contextPath}/admin-resources/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="${pageContext.request.contextPath}/admin-resources/css/bootstrap-theme.css" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="${pageContext.request.contextPath}/admin-resources/css/elegant-icons-style.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/admin-resources/css/font-awesome.min.css" rel="stylesheet"/>
    <!-- full calendar css-->
    <link href="${pageContext.request.contextPath}/admin-resources/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css"
          rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/admin-resources/assets/fullcalendar/fullcalendar/fullcalendar.css"
          rel="stylesheet"/>
    <!-- easy pie chart-->
    <link href="${pageContext.request.contextPath}/admin-resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css"
          rel="stylesheet" type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/admin-resources/css/owl.carousel.css"
          type="text/css">
    <link href="${pageContext.request.contextPath}/admin-resources/css/jquery-jvectormap-1.2.2.css" rel="stylesheet">
    <!-- Custom styles -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/admin-resources/css/fullcalendar.css">
    <link href="${pageContext.request.contextPath}/admin-resources/css/widgets.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin-resources/css/style.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/admin-resources/css/style-responsive.css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/admin-resources/css/xcharts.min.css" rel=" stylesheet">
    <link href="${pageContext.request.contextPath}/admin-resources/css/jquery-ui-1.10.4.min.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
    <script src="${pageContext.request.contextPath}/admin-resources/js/html5shiv.js"></script>
    <script src="${pageContext.request.contextPath}/admin-resources/js/respond.min.js"></script>
    <script src="${pageContext.request.contextPath}/admin-resources/js/lte-ie7.js"></script>
    <![endif]-->
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/admin-resources/assets/ckeditor/ckeditor.js"></script>

</head>

<body>
<!-- container section start -->
<section id="container" class="">


    <%@include file="fragment/nav-bar.jspf" %>
    <!--header end-->

    <!--sidebar start-->
    <%@include file="fragment/left-menu.jspf" %>

    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!--overview start-->
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><i class="fa fa-laptop"></i> Dashboard</h3>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="index.jsp">Home</a></li>
                        <li><i class="fa fa-laptop"></i>Dashboard</li>
                    </ol>
                    <c:if test="${success}">
                    <div class="alert alert-success fade in">
                        <button data-dismiss="alert" class="close close-sm" type="button">
                            <i class="icon-remove"></i>
                        </button>
                        <strong> </strong> Квартира была успешно обновлена
                    </div>
                    </c:if>
                </div>
            </div>

            <form:form method="post" action="${pageContext.request.contextPath}/flat/update" commandName="flat">

                <div class="row">
                    <section class="panel">
                        <div class="panel-body">
                                <div class="form-group">
                                    <input type="hidden" name="id" value="${flat.id}">
                                    <label class="control-label col-lg-2">Тип предоставления</label>
                                    <div class="col-lg-10">
                                        <div class="radio">
                                            <label>
                                                <form:radiobutton path="flatOrder" id="optionsRadios1" value="RENT"/>
                                                Аренда
                                            </label>
                                        </div>
                                        <div class="radio">
                                            <label>
                                                <form:radiobutton path="flatOrder" id="optionsRadios1" value="SELLING"/>
                                                Продажа
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-2">Количество комнат</label>
                                    <div class="col-lg-10">
                                        <label class="fc-day-number">

                                            <form:input path="rooms" value="${flat.rooms}"/>
                                            <form:errors path="rooms" />
                                        </label>
                                    </div>
                                </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Размер</label>
                                <div class="col-lg-10">
                                    <label class="fc-day-number">
                                        <form:input path="size" value="${flat.size}"/>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Улица</label>
                                <div class="col-lg-10">
                                    <label class="fc-day-number">
                                        <form:input path="street" vlaue="${flat.street}" />
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-2">Стоимость</label>
                                <div class="col-lg-10">
                                    <label class="fc-day-number">
                                        <form:input path="cost" value="${flat.cost}" />
                                    </label>
                                </div>
                            </div>
                            <%--<div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">  </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input id="photo" class="date-picker form-control col-md-7 col-xs-12" type="file" name="photo" accept="image/jpeg">
                                </div>
                            </div>--%>
                        </div>
                    </section>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <section class="panel">
                            <header class="panel-heading">
                                Описание
                            </header>
                            <div class="panel-body">
                                <div class="form">
                                        <div class="form-group">
                                            <label class="control-label col-sm-2"></label>
                                            <div class="col-sm-10">
                                                <form:textarea path="description" cssClass="form-control ckeditor"
                                                    rows="6"/>
                                            </div>
                                        </div>
                                        <input type="submit" class="btn btn-primary" value="Update"/>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </form:form>
        </section>
    </section>
    <!--main content end-->
</section>
<!-- container section start -->

<!-- javascripts -->
<script src="${pageContext.request.contextPath}/admin-resources/js/jquery.js"></script>
<script src="${pageContext.request.contextPath}/admin-resources/js/jquery-ui-1.10.4.min.js"></script>
<script src="${pageContext.request.contextPath}/admin-resources/js/jquery-1.8.3.min.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/admin-resources/js/jquery-ui-1.9.2.custom.min.js"></script>
<!-- bootstrap -->
<script src="${pageContext.request.contextPath}/admin-resources/js/bootstrap.min.js"></script>
<!-- nice scroll -->
<script src="${pageContext.request.contextPath}/admin-resources/js/jquery.scrollTo.min.js"></script>
<script src="${pageContext.request.contextPath}/admin-resources/js/jquery.nicescroll.js"
        type="text/javascript"></script>
<!-- charts scripts -->
<script src="${pageContext.request.contextPath}/admin-resources/assets/jquery-knob/js/jquery.knob.js"></script>
<script src="${pageContext.request.contextPath}/admin-resources/js/jquery.sparkline.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/admin-resources/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="${pageContext.request.contextPath}/admin-resources/js/owl.carousel.js"></script>
<!-- jQuery full calendar -->
<
<script src="${pageContext.request.contextPath}/admin-resources/js/fullcalendar.min.js"></script>
<!-- Full Google Calendar - Calendar -->
<script src="${pageContext.request.contextPath}/admin-resources/assets/fullcalendar/fullcalendar/fullcalendar.js"></script>
<!--script for this page only-->
<script src="${pageContext.request.contextPath}/admin-resources/js/calendar-custom.js"></script>
<script src="${pageContext.request.contextPath}/admin-resources/js/jquery.rateit.min.js"></script>
<!-- custom select -->
<script src="${pageContext.request.contextPath}/admin-resources/js/jquery.customSelect.min.js"></script>
<script src="${pageContext.request.contextPath}/admin-resources/assets/chart-master/Chart.js"></script>

<!--custome script for all page-->
<script src="${pageContext.request.contextPath}/admin-resources/js/scripts.js"></script>
<!-- custom script for this page-->
<script src="${pageContext.request.contextPath}/admin-resources/js/sparkline-chart.js"></script>
<script src="${pageContext.request.contextPath}/admin-resources/js/easy-pie-chart.js"></script>
<script src="${pageContext.request.contextPath}/admin-resources/js/jquery-jvectormap-1.2.2.min.js"></script>
<script src="${pageContext.request.contextPath}/admin-resources/js/jquery-jvectormap-world-mill-en.js"></script>
<script src="${pageContext.request.contextPath}/admin-resources/js/xcharts.min.js"></script>
<script src="${pageContext.request.contextPath}/admin-resources/js/jquery.autosize.min.js"></script>
<script src="${pageContext.request.contextPath}/admin-resources/js/jquery.placeholder.min.js"></script>
<script src="${pageContext.request.contextPath}/admin-resources/js/gdp-data.js"></script>
<script src="${pageContext.request.contextPath}/admin-resources/js/morris.min.js"></script>
<script src="${pageContext.request.contextPath}/admin-resources/js/sparklines.js"></script>
<script src="${pageContext.request.contextPath}/admin-resources/js/charts.js"></script>
<script src="${pageContext.request.contextPath}/admin-resources/js/jquery.slimscroll.min.js"></script>
<script>

    //knob
    $(function () {
        $(".knob").knob({
            'draw': function () {
                $(this.i).val(this.cv + '%')
            }
        })
    });

    //carousel
    $(document).ready(function () {
        $("#owl-slider").owlCarousel({
            navigation: true,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true

        });
    });

    //custom select box

    $(function () {
        $('select.styled').customSelect();
    });

    /* ---------- Map ---------- */
    $(function () {
        $('#map').vectorMap({
            map: 'world_mill_en',
            series: {
                regions: [{
                    values: gdpData,
                    scale: ['#000', '#000'],
                    normalizeFunction: 'polynomial'
                }]
            },
            backgroundColor: '#eef3f7',
            onLabelShow: function (e, el, code) {
                el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
            }
        });
    });


</script>

</body>
</html>
