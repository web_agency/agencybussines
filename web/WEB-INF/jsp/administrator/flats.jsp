<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Basic Table | Creative - Bootstrap 3 Responsive Admin Template</title>

    <%@include file="fragment/header.jspf" %>
</head>

<body>
<%@include file="fragment/nav-bar.jspf" %>
<%@include file="fragment/left-menu.jspf" %>

<!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        Квартиры
                    </header>
                    <div class="row">
                        <div class="col-lg-12">
                            <c:if test="${success}">
                                <div class="alert alert-success fade in">
                                    <button data-dismiss="alert" class="close close-sm" type="button">
                                        <i class="icon-remove"></i>
                                    </button>
                                    Квартира была успешно удалена
                                </div>
                            </c:if>
                        </div>
                    </div>

                    <table class="table table-striped table-advance table-hover">
                        <tbody>
                        <tr>
                            <th><i class="icon_pin_alt"></i> Улица</th>
                            <th><i class="icon_zoom-in"></i> Размер</th>
                            <th><i class="icon_creditcard"></i> Стоимость</th>
                            <th><i class="icon_building"></i> Кол-во комнат</th>
                            <th></th>
                        </tr>
                        <c:forEach var="flat" items="${flats}">
                            <tr>
                                <td>${flat.street}</td>
                                <td>${flat.size}</td>
                                <td>${flat.cost}</td>
                                <td>${flat.rooms}</td>
                                <td>
                                    <div class="btn-group">
                                       <%-- <a class="btn btn-warning" href="#"><i class="icon_pencil-edit"></i></a>--%>
                                        <form:form method="post" action="${pageContext.request.contextPath}/flat/prepareForEditing" commandName="prepareForEditing">
                                            <input type="hidden" value="${flat.id}" name="id"/>
                                            <input type="submit" class="btn btn-warning" value="Обновить" />
                                        </form:form>
                                        <form:form method="post" action="${pageContext.request.contextPath}/flat/delete" commandName="delete">
                                            <input type="hidden" value="${flat.id}" name="id"/>
                                            <input type="submit" class="btn btn-danger" value=" Удалить " style="width: 89px; height: 34px"/>
                                        </form:form>
                                    </div>

                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                </section>
            </div>
        </div>
        <!-- page end-->
    </section>
</section>


</body>
</html>
