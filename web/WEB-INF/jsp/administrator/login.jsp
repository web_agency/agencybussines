<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">


    <title>Creative - Bootstrap Admin Template</title>
    <%@include file="fragment/header.jspf" %>
</head>


<body class="login-img3-body">
<!-- container section start -->
  <spring:url var="authUrl" value="/j_spring_security_check" />
    <div class="container">

      <form class="login-form" action="${pageContext.request.contextPath}/j_spring_security_check" method="post" role="form">
        <div class="login-wrap">
            <p class="login-img"><i class="icon_lock_alt"></i></p>
            <div class="input-group">
              <span class="input-group-addon"><i class="icon_profile"></i></span>
                <input  id="login" type="text" class="form-control" name="login"
                        placeholder="Login" required />
            </div>
            <div class="input-group">
                <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                <input type="password" class="form-control" name="password"  placeholder="Password" required />
            </div>
            <button type="submit" id="submit" class="btn btn-success">Login</button>
        </div>
      </form>



  </div>


  </body>
</html>
