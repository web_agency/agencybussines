package ua.agency.entity.form;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ua.agency.entity.Flat;

public class FlatValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return Flat.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"rooms","flat.rooms");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"cost","flat.cost");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"size","flat.size");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"street","flat.street");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"flatOrder","flat.flatOrder");
    }
}
