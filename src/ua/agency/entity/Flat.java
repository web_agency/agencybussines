package ua.agency.entity;

public class Flat extends Entity{

    private Short rooms;
    private Short cost;
    private String description;
    private Short size;
    private String street;
    private FlatOrder flatOrder;

    public Flat(Long id) {
        super(id);
    }

    public Flat(){
    }

    public Short getSize() {
        return size;
    }

    public void setSize(Short size) {
        this.size = size;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Short getRooms() {
        return rooms;
    }

    public void setRooms(Short rooms) {
        this.rooms = rooms;
    }

    public Short getCost() {
        return cost;
    }

    public void setCost(Short cost) {
        this.cost = cost;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public FlatOrder getFlatOrder() {
        return flatOrder;
    }

    public void setFlatOrder(FlatOrder flatOrder) {
        this.flatOrder = flatOrder;
    }
}
