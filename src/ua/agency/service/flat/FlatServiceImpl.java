package ua.agency.service.flat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.agency.dao.FlatDao;
import ua.agency.entity.Entity;
import ua.agency.entity.Flat;

import java.util.List;

@Service
@Transactional
public class FlatServiceImpl implements FlatService {

    @Autowired
    private FlatDao flatDao;

    @Override
    public void save(Flat flat){
        flatDao.save(flat);
    }

    @Override
    public void delete(Flat flat) {
        flatDao.delete(flat);
    }

    @Override
    public void update(Flat flat) {
        flatDao.update(flat);
    }

    @Override
    public List<Flat> getRentFlats() {
        return flatDao.getRentFlats();
    }

    @Override
    public List<Flat> getSellingFlats() {
        return flatDao.getSellingFlats();
    }

    @Override
    public Flat getFlatInfo(Long id) {
        return flatDao.getFlatInfo(id);
    }

    @Override
    public Flat getFlat(Long id) {
        return flatDao.getFlat(id);
    }

}
