package ua.agency.service.flat;

import ua.agency.entity.Flat;

import java.util.List;

public interface FlatService {

    void save(Flat flat);

    void delete(Flat flat);

    void update(Flat flat);

    List<Flat> getRentFlats();

    List<Flat> getSellingFlats();

    Flat getFlatInfo(Long id);

    Flat getFlat(Long id);
}
