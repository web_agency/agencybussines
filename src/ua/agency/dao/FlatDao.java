package ua.agency.dao;

import ua.agency.entity.Flat;

import java.util.List;

public interface FlatDao {

    void save(Flat flat);

    void delete(Flat flat);

    void update(Flat flat);

    List getRentFlats();

    List getSellingFlats();

    Flat getFlatInfo(Long id);

    Flat getFlat(Long id);
}
