package ua.agency.dao.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public final class SessionUtil {

    private SessionUtil(){}

    public static Session getSession(SessionFactory sessionFactory){
        if(sessionFactory.isClosed())
            return sessionFactory.openSession();
        else
            return sessionFactory.getCurrentSession();
    }

}
