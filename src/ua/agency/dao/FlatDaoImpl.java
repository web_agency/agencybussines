package ua.agency.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.agency.dao.util.SessionUtil;
import ua.agency.entity.Flat;
import ua.agency.entity.FlatOrder;

import java.util.List;

@Repository
public class FlatDaoImpl implements FlatDao{

    @Autowired
    private SessionFactory sessionFactory;

    public void save(Flat flat) {
        Session session = SessionUtil.getSession(sessionFactory);
        session.beginTransaction();
        session.save(flat);
        session.getTransaction().commit();
    }

    @Override
    public void delete(Flat flat) {
        Session session = SessionUtil.getSession(sessionFactory);
        session.beginTransaction();
        session.delete(flat);
        session.getTransaction().commit();
    }

    @Override
    public void update(Flat flat) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.update(flat);
        session.getTransaction().commit();
        if(session.isOpen())
            session.close();
    }

    @Override
    public List<Flat> getRentFlats() {
        Session session = SessionUtil.getSession(sessionFactory);
        session.beginTransaction();

        Criteria criteria = session.createCriteria(Flat.class);
        criteria.add(Restrictions.eq("flatOrder", FlatOrder.RENT));
        List<Flat> flats = criteria.list();
        session.getTransaction().commit();
        return flats;
    }

    @Override
    public List<Flat> getSellingFlats() {
        Session session = SessionUtil.getSession(sessionFactory);
        session.beginTransaction();

        Criteria criteria = session.createCriteria(Flat.class);
        criteria.add(Restrictions.eq("flatOrder", FlatOrder.SELLING));
        List<Flat> flats = criteria.list();
        session.getTransaction().commit();
        return flats;
    }

    @Override
    public Flat getFlatInfo(Long id) {
        Session session = SessionUtil.getSession(sessionFactory);
        session.beginTransaction();
        Flat flat = (Flat)session.get(Flat.class, id);
        session.getTransaction().commit();
        return flat;
    }

    @Override
    public Flat getFlat(Long id) {
        Session session = SessionUtil.getSession(sessionFactory);
        session.beginTransaction();
        Flat flat = (Flat)session.get(Flat.class, id);
        session.getTransaction().commit();
        return flat;
    }
}
