package ua.agency.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ua.agency.core.mail.MailSender;
import ua.agency.core.mail.Message;

@Controller
@RequestMapping("/mail")
public class MailController {

    @Autowired
    private MailSender mailSender;

    @RequestMapping("/contact")
    public ModelAndView contact(){
        return new ModelAndView("contact","message", new Message());
    }

    @RequestMapping("/request")
    public ModelAndView sendMail(@ModelAttribute Message message){
        boolean result = mailSender.send(message);
        return new ModelAndView("");
    }

    @RequestMapping(value = "/feedback", method = RequestMethod.POST)
    public ModelAndView feedback(@ModelAttribute Message message){
        message.setSubject("Вопрос");
        message.setFrom("question");
        boolean result = mailSender.send(message);
        return new ModelAndView("contact","success",result);
    }

}
