package ua.agency.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ua.agency.entity.Flat;
import ua.agency.service.flat.FlatService;

import java.util.List;

@Controller
@RequestMapping("/flat")
public class FlatController {

    @Autowired
    private FlatService flatService;

    @Autowired
    @Qualifier("flatValidator")
    private Validator validator;

    @InitBinder
    private void initBinder(WebDataBinder webDataBinder){
        webDataBinder.setValidator(validator);
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ModelAndView saveFlat(@ModelAttribute @Validated Flat flat, BindingResult bindingResult){
        /*if(bindingResult.hasErrors()){
            return new ModelAndView("administrator/add-flat");
        }*/
        flatService.save(flat);
        return new ModelAndView("administrator/add-flat","success", true);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public ModelAndView deleteFlat(@ModelAttribute Flat flat){
        flatService.delete(flat);
        return new ModelAndView("administrator/flats","success", true);
    }
    @RequestMapping(value = "/rent", method = RequestMethod.GET)
    public ModelAndView getRentFlats(){
        List<Flat> flatList = flatService.getRentFlats();

        ModelAndView modelAndView = new ModelAndView("rent");
        modelAndView.addObject("flats", flatList);

        return modelAndView;
    }

    @RequestMapping(value = "/selling", method = RequestMethod.GET)
    public ModelAndView getSellingFlats(){
        List<Flat> flatList = flatService.getSellingFlats();

        ModelAndView modelAndView = new ModelAndView("sale");
        modelAndView.addObject("flats", flatList);

        return modelAndView;
    }

    @RequestMapping(value = "/info/{id}", method = RequestMethod.GET)
    public ModelAndView getFlatInfo(@PathVariable Long id){
        Flat flat = flatService.getFlatInfo(id);
        ModelAndView modelAndView = new ModelAndView("rent-flat-info");
        modelAndView.addObject("flat", flat);
        return modelAndView;
    }

    // Нужен метод для редактирвоания
    @RequestMapping(value = "/prepareForEditing", method = RequestMethod.POST)
    public ModelAndView prepareForEditing(Long id){
        Flat flat = flatService.getFlat(id);
        ModelAndView modelAndView = new ModelAndView("administrator/edit-flat");
        modelAndView.addObject("flat", flat);
        return modelAndView;
    }

    @RequestMapping(value="/update", method = RequestMethod.POST)
    public ModelAndView editFlat(@ModelAttribute Flat flat){
         flatService.update(flat);
         return new ModelAndView("administrator/edit-flat","success", true);
    }
}
