package ua.agency.web.controller;

import com.sun.javafx.sg.PGShape;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ua.agency.entity.Flat;
import ua.agency.entity.FlatOrder;
import ua.agency.service.flat.FlatService;

import java.util.List;

@Controller
@RequestMapping("/administrator")
public class AdministratorController {

    @Autowired
    private FlatService flatService;

    @RequestMapping("/panel")
    public ModelAndView adminPanel(){
        return new ModelAndView("administrator/index");
    }

    @RequestMapping("/login")
    public ModelAndView loginPage(){
        return  new ModelAndView("administrator/login");
    }

    @RequestMapping("/add-flat")
    public ModelAndView flatPage(){
        ModelAndView modelAndView = new ModelAndView("administrator/add-flat","flat",new Flat());
        return modelAndView;
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView deleteFlat(@ModelAttribute Flat flat) {
            flatService.delete(flat);
            return new ModelAndView("administrator/flats","success", true);
        }


    @RequestMapping(value = "/rent", method = RequestMethod.GET)
    public ModelAndView getRentFlats(){
        List<Flat> flatList = flatService.getRentFlats();

        ModelAndView modelAndView = new ModelAndView("administrator/flats");
        modelAndView.addObject("flats", flatList);

        return modelAndView;
    }

    @RequestMapping(value = "/selling", method = RequestMethod.GET)
    public ModelAndView getSellingFlats(){
        List<Flat> flatList = flatService.getSellingFlats();

        ModelAndView modelAndView = new ModelAndView("administrator/flats");
        modelAndView.addObject("flats", flatList);

        return modelAndView;
    }

}
